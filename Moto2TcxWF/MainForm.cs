﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;
using net.melastmohican.moto2tcx;

namespace net.melastmohican.moto2tcx.win
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void buttonBrowse_Click(object sender, EventArgs e)
        {
            if (openCSVFile.ShowDialog() == DialogResult.OK)
            {
                textFilename.Text = openCSVFile.FileName;
            }
        }

        private void textFilename_TextChanged(object sender, EventArgs e)
        {
            buttonConvert.Enabled = !String.IsNullOrEmpty(textFilename.Text);
        }

        private void buttonConvert_Click(object sender, EventArgs e)
        {
            string selectedDir = System.IO.Path.GetDirectoryName(textFilename.Text);
            string selectedFile = System.IO.Path.GetFileNameWithoutExtension(textFilename.Text) + ".tcx";
            string outputFile = System.IO.Path.Combine(selectedDir, selectedFile);

            Moto2TCX m2t = new Moto2TCX(textFilename.Text);
            XDocument result = m2t.convert();
            result.Save(outputFile);
			m2t.validate (outputFile);
        }


    }
}
