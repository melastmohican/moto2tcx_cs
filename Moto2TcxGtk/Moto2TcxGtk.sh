#!/bin/bash 
MONO_FRAMEWORK=/Library/Frameworks/Mono.framework/Versions/Current/
export DYLD_FALLBACK_LIBRARY_PATH="$MONO_FRAMEWORK/lib:/usr/local/lib:/usr/lib"
$MONO_FRAMEWORK/bin/mono bin/Release/Moto2TcxGUI.exe "$@"