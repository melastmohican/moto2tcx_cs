﻿using System;
using System.IO;
using Gtk;
using System.Xml;
using System.Xml.Linq;
using net.melastmohican.moto2tcx;

public partial class MainWindow: Gtk.Window
{
	public MainWindow () : base (Gtk.WindowType.Toplevel)
	{
		Build ();

		FileFilter filter = new FileFilter ();
		filter.Name = "MotoACTV files";
		filter.AddPattern ("*.csv");
		selectFile.AddFilter (filter);

	}

	protected void OnDeleteEvent (object sender, DeleteEventArgs a)
	{
		Application.Quit ();
		a.RetVal = true;
	}

	protected void OnConvertButtonClicked (object sender, EventArgs e)
	{
		string selectedDir = System.IO.Path.GetDirectoryName (selectFile.Filename);
		string selectedFile = System.IO.Path.GetFileNameWithoutExtension(selectFile.Filename) + ".tcx";
		string outputFile = System.IO.Path.Combine (selectedDir, selectedFile);

		Moto2TCX m2t = new Moto2TCX (selectFile.Filename);
		XDocument result = m2t.convert ();
		result.Save (outputFile);
		m2t.validate (outputFile);

	}
		
	protected void OnSelectFileSelectionChanged (object sender, EventArgs e)
	{
		convertButton.Sensitive = true;
	}
}
