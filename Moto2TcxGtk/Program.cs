﻿using System;
using Gtk;

namespace net.melastmohican.moto2tcx.gtk
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			Application.Init ();
			MainWindow win = new MainWindow ();
			win.Show ();
			Application.Run ();
		}
	}
}
