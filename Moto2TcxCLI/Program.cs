﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using net.melastmohican.moto2tcx;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;

namespace net.melastmohican.moto2tcx.cli
{
	class Program
	{
		static void Main (string[] args)
		{
			FileInfo fi = new FileInfo (args [0]);
			string dir = Path.GetDirectoryName (fi.FullName);
			string filename = Path.GetFileNameWithoutExtension (fi.FullName) + ".tcx";
			Moto2TCX m2t = new Moto2TCX (fi.ToString ());
			XDocument ouputDoc = m2t.convert ();
			string output = Path.Combine (dir, filename);
			ouputDoc.Save (output);

			XmlSchemaSet schemas = new XmlSchemaSet ();
			schemas.Add ("http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2", Path.Combine (dir, "TrainingCenterDatabasev2.xsd"));

			Console.WriteLine ("Attempting to validate");
			bool errors = false;
			ouputDoc.Validate (schemas, (o, e) => {
				Console.WriteLine ("{0}", e.Message);
				errors = true;
			});
			Console.WriteLine ("Document {0} - {1}", output, errors ? "did not validate" : "validated");
		}
	}
}
