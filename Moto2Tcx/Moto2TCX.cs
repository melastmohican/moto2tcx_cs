﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;

namespace net.melastmohican.moto2tcx
{
	public class Moto2TCX
	{
		public enum Activities
		{
			Running = 0,
			Biking = 1,
			Other = 2
		}

		private string input;
		// "DISTANCE","activity_id","HEARTRATE","SPEED","STEPS_PER_MINUTE","LATITUDE","repetitions","temperature","INSTANT_TORQUE_CRANK",
		// "timestamp_epoch","ELEVATION","POWER","STRIDES","wheel_torque","CALORIEBURN","LONGITUDE","CADENCE","heading","STEP_RATE"
		public Moto2TCX (string input)
		{
			this.input = input;
		}

		public XDocument convert ()
		{
			// Read into an array of strings.
			string[] csv = File.ReadAllLines (input);
			// remove quotes
			string[] headers = (from header in csv [0].Split (',')
			                    where !string.IsNullOrEmpty (header)
			                    select header.Trim ('"')).ToArray ();

			var lines = (from line in csv.Skip (1)
			             let fields = (from field in line.Split (',')
			                           select field.Trim ('"')).ToArray ()
			             select (from header in headers
			                     select new
                                 {
                                     Key = header,
                                     Value = fields [Array.IndexOf (headers, header)]
                                 }
			                 ).ToDictionary (p => p.Key, p => p.Value)
			            ).ToArray ();

			// calculations
			int size = lines.Length;
			Dictionary<string, string> firstLine = lines [0];
			Dictionary<string, string> lastLine = lines [size - 1];
			int avgHR = Convert.ToInt32 (lines.Average (line => double.Parse (line ["HEARTRATE"])));
			int maxHR = Convert.ToInt32 (lines.Max (line => double.Parse (line ["HEARTRATE"])));
			double maxSpeed = lines.Max (line => double.Parse (line ["SPEED"]));
			long start = long.Parse (firstLine ["timestamp_epoch"]);
			long end = long.Parse (lastLine ["timestamp_epoch"]);
			long total = (end - start) / 1000;
			double distance = double.Parse (lastLine ["DISTANCE"]);
			int calories = Convert.ToInt32 (Double.Parse (lastLine ["CALORIEBURN"]));


			XNamespace ns = "http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2";
			XNamespace xsi = "http://www.w3.org/2001/XMLSchema-instance";
			XElement root = new XElement (ns + "TrainingCenterDatabase",
				                new XAttribute (XNamespace.Xmlns + "xsi", xsi),
				                new XAttribute (xsi + "schemaLocation", "http://www.garmin.com/xmlschemas/ActivityExtension/v2 http://www.garmin.com/xmlschemas/ActivityExtensionv2.xsd http://www.garmin.com/xmlschemas/FatCalories/v1 http://www.garmin.com/xmlschemas/fatcalorieextensionv1.xsd http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2 http://www.garmin.com/xmlschemas/TrainingCenterDatabasev2.xsd"),
				                new XElement (ns + "Folders"),
				                new XElement (ns + "Activities",
					                new XElement (ns + "Activity",
						                new XAttribute ("Sport", GetActivity (int.Parse (firstLine ["activity_id"]))),
						                new XElement (ns + "Id", ToIso8601 (firstLine ["timestamp_epoch"])),
						                new XElement (ns + "Lap",
							                new XAttribute ("StartTime", ToIso8601 (firstLine ["timestamp_epoch"])),
							                new XElement (ns + "TotalTimeSeconds", total),
							                new XElement (ns + "DistanceMeters", distance),
							                new XElement (ns + "MaximumSpeed", maxSpeed),
							                new XElement (ns + "Calories", calories),
							                new XElement (ns + "AverageHeartRateBpm",
								                new XElement (ns + "Value", avgHR)),
							                new XElement (ns + "MaximumHeartRateBpm",
								                new XElement (ns + "Value", maxHR)),
							                new XElement (ns + "Intensity", "Active"),
							                new XElement (ns + "TriggerMethod", "Distance"),
							                new XElement (ns + "Track",
								                from line in lines
								                select new XElement (ns + "Trackpoint",
									                    new XElement (ns + "Time", ToIso8601 (line ["timestamp_epoch"])),
									                    new XElement (ns + "Position",
										                    new XElement (ns + "LatitudeDegrees", line ["LATITUDE"]),
										                    new XElement (ns + "LongitudeDegrees", line ["LONGITUDE"])),
									                    new XElement (ns + "AltitudeMeters", line ["ELEVATION"]),
									                    new XElement (ns + "DistanceMeters", line ["DISTANCE"]),
									                    new XElement (ns + "HeartRateBpm", new XElement (ns + "Value", double.Parse (line ["HEARTRATE"]))),
									                    new XElement (ns + "Cadence", line ["CADENCE"]),
									                    new XElement (ns + "SensorState", "Absent")
								                    ))),
						                new XElement (ns + "Creator",
							                new XAttribute (xsi + "type", "Device_t"),
							                new XElement (ns + "Name", "melastmohican"),
							                new XElement (ns + "UnitId", 7),
							                new XElement (ns + "ProductID", 7),
							                new XElement (ns + "Version",
								                new XElement (ns + "VersionMajor", 1),
								                new XElement (ns + "VersionMinor", 0),
								                new XElement (ns + "BuildMajor", 1),
								                new XElement (ns + "BuildMinor", 0)
							                )
						                )
					                )),
				                
				                
				                new XElement (ns + "Workouts"),
				                new XElement (ns + "Courses"),
				                new XElement (ns + "Author",
					                new XAttribute (xsi + "type", "Application_t"),
					                new XElement (ns + "Name", "Garmin Training Center"),
					                new XElement (ns + "Build",
						                new XElement (ns + "Version",
							                new XElement (ns + "VersionMajor", 1),
							                new XElement (ns + "VersionMinor", 0),
							                new XElement (ns + "BuildMajor", 1),
							                new XElement (ns + "BuildMinor", 0)
						                ),
						                new XElement (ns + "Type", "Release"),
						                new XElement (ns + "Time", "Jan  1 2012, 22:00:00"),
						                new XElement (ns + "Builder", "melastmohican")),
					                new XElement (ns + "LangID", "en"),
					                new XElement (ns + "PartNumber", "006-A0183-00")
				                ));


			// write to file
			return new XDocument (new XDeclaration ("1.0", "utf-8", null), root);
		}

		private string GetActivity (int activity)
		{
			switch (activity) {
			case 1:
			case 2:
				return Activities.Running.ToString ();
			case 4:
				return Activities.Biking.ToString ();
			default:
				return Activities.Other.ToString ();
			}
		}

		private string ToIso8601 (string epoch)
		{
			long le = long.Parse (epoch);
			DateTime dt = new DateTime (1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddMilliseconds (le);
			return dt.ToString ("s") + "Z";
		}

		private XmlSchemaSet LoadSchema (string tag)
		{
			XmlSchemaSet schemas = new XmlSchemaSet ();
			string resource = String.Format ("net.melastmohican.moto2tcx.{0}", tag);
			using (Stream schemaStream = Assembly.GetCallingAssembly ().GetManifestResourceStream (resource)) {
				using (XmlReader schemaReader = XmlReader.Create (schemaStream)) {
					schemas.Add (null, schemaReader);
					return schemas;
				}
			}

		}

		public bool validate (XDocument xmlDoc)
		{
			XmlSchemaSet schemas = LoadSchema ("TrainingCenterDatabasev2.xsd");
			bool errors = false;
			xmlDoc.Validate (schemas, (o, e) => {
				Console.WriteLine ("{0}", e.Message);
				errors = true;
			});
			Console.WriteLine ("Document {0}", errors ? "did not validate" : "validated");
			return (errors == false);
		}

		public bool validate (XDocument xmlDoc, string schemaFile)
		{
			XmlSchemaSet schemas = new XmlSchemaSet ();
			schemas.Add ("http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2", schemaFile);
			bool errors = false;
			xmlDoc.Validate (schemas, (o, e) => {
				Console.WriteLine ("{0}", e.Message);
				errors = true;
			});
			Console.WriteLine ("Document {0}", errors ? "did not validate" : "validated");
			return (errors == false);
		}

		public void validate(string filename, XmlSchemaSet schemaSet) {
			Console.WriteLine ("Validating XML file {0}...", filename.ToString ());
			XmlSchema compiledSchema = null;

			foreach (XmlSchema schema in schemaSet.Schemas()) {
				compiledSchema = schema;
			}

			XmlReaderSettings settings = new XmlReaderSettings ();
			settings.Schemas.Add (compiledSchema);
			settings.ValidationEventHandler += new ValidationEventHandler (ValidationCallBack);
			settings.ValidationType = ValidationType.Schema;

			//Create the schema validating reader.
			using (XmlReader vreader = XmlReader.Create (filename, settings)) {
				while (vreader.Read ()) {
				}

			}
			Console.WriteLine ("Validating XML file {0} done", filename.ToString ());
		}

		public void validate(string filename) {
			XmlSchemaSet schemaSet = LoadSchema ("TrainingCenterDatabasev2.xsd");
			validate (filename, schemaSet);
		}

		public void validate (String filename, string schemaFile)
		{
			XmlSchemaSet schemaSet = new XmlSchemaSet ();
			schemaSet.Add ("http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2", schemaFile);
			validate (filename, schemaSet);
		}

		//Display any warnings or errors.
		private void ValidationCallBack (object sender, ValidationEventArgs args)
		{
			if (args.Severity == XmlSeverityType.Warning)
				Console.WriteLine ("\tWarning: Matching schema not found.  No validation occurred." + args.Message);
			else
				Console.WriteLine ("\tValidation error: " + args.Message);

		}
	}
}
